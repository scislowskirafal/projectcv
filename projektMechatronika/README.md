# README #
Autor: Rafał Ścisłowski
Repozytorium dla aplikacji do komputerowej wizji.

### Do czego jest to repozytorium? ###

* Folder modules/ zawiera moduły do wykrywania i oznaczania obiektów: dłoni, twarzy i ciała.
* Folder projects/ zawiera inne projekty z komputerowej wizji.
* Ffolder robotVision/ zawiera projekt na zaliczenie.
* Podfolder robotVision/src/ zawiera kody do robienia pokazowych plików .gif i firmatę do Arduino w języku C.
* Plik robotVision/run.py jest głównym plikiem projektu, w nim uruchamiam program działający z moim urządzeniem.

### Propozycja uruchomienia ###

1.	Uruchamiam Pycharma
2.	Podłączam Arduino i camerę do portów USB
3.	Uruchamiam plik arduino_part.ino w Arduino IDE jako zewnętrzne narzędzie w pycharmie
4.	Klikam run
5.	Uruchamiam zasilanie- pudełko baterii
6.	Uruchamiam program SplitCam, gdyż windows nie pozwala na dzielenie obrazu z kamery między wieloma aplikacjami, np. między moim programem i zoomem.
	Tam tworzę dwie kopie  obrazu z kamery dzielone przez dwie aplikacje
7.	Uruchamiam kod run.py z odpowiednimi parametrami funkcji.
8.	Aplikacja działa, można korzystać

### Pokazowe pliki .gif ###
----------------------
1. Video z onedrive, robot i sposób użycia<br/>
Pod tym linkiem można obejrzeć działanie robota:<br/>
<!-- [![Watch the video HERE]()](https://politechnikawroclawska-my.sharepoint.com/:v:/g/personal/262058_student_pwr_edu_pl/EcnU1x-kc5ZFpk9yIXDqqEQBkQ6v_n6W642Gosv1ugSSuw?e=TNY89O) -->
[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/YOUTUBE_VIDEO_ID_HERE/0.jpg)](https://www.youtube.com/watch?v=IU-B1om9FFs)
----------------------
2. GIF z kamery dynamicznej.<br/>
projektMechatronika/robotVision/tests/gifs/film2.gif<br/>
![GIF:](https://bitbucket.org/scislowskirafal/projectcv/raw/66deffce3c919bf5989e7c6c63729c7a19dc19b9/projektMechatronika/robotVision/tests/gifs/film2.gif)<br/>
----------------------
3. Gif z kamery statycznej 33 minuty, 100 klatek.<br/>
projektMechatronika/robotVision/tests/gifs/film1.gif<br/>
![GIF:](https://bitbucket.org/scislowskirafal/projectcv/raw/66deffce3c919bf5989e7c6c63729c7a19dc19b9/projektMechatronika/robotVision/tests/gifs/film1.gif)<br/>
----------------------