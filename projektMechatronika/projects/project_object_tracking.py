from projektMechatronika.modules import \
    HandTrackingModule, \
    FaceMeshModule, \
    PoseModule
# from computerVision.robotVision.ComponentsPart import RoboServo1
# MyServo=RoboServo1.MyServo
# from computerVision.robotVision.ComponentsPart import ServoMotor
import cv2
import numpy as np
import pyfirmata
import time

print("test1")
board = pyfirmata.Arduino('COM3')
print("test2")


class TrackingClass:
    def __init__(self, n_camera):
        """
        Klasa wykonująca i zapisująca zdjęcia

        :param finite:
        :returns: this is a description of what is returned
        :raises keyError: raises an exception
        """
        self.wCam, self.hCam = 640, 480
        self.midWCam, self.midHCam = 320, 240
        self.cap = cv2.VideoCapture(n_camera)
        self.face_detector = FaceMeshModule.FaceMeshDetector(maxFaces=1)
        self.hand_detector = HandTrackingModule.handDetector(maxHands=1)
        self.pose_detector = PoseModule.poseDetector()
        self.pTime = 0
        self.cTime = 0

    def recoring(self, option="sport"):
        verti_save = 90
        hori_save = 90
        angle_verti = 90
        angle_hori = 90
        servo = 0
        # NSEW
        hitbox = {}
        object_hand = []
        object_face = []
        while True:
            if option == "sport":
                hitbox["N"] = self.midHCam + 140
                hitbox["S"] = self.midHCam - 140
                hitbox["E"] = self.midWCam + 200
                hitbox["W"] = self.midWCam - 200
            if option == "show":
                hitbox["N"] = self.midHCam + 100
                hitbox["S"] = self.midHCam - 100
                hitbox["E"] = self.midWCam + 100
                hitbox["W"] = self.midWCam - 100
            servo += 10
            if angle_verti <= 1:
                angle_verti = 1
            if angle_verti >= 180:
                angle_verti = 180
            if angle_hori <= 1:
                angle_hori = 1
            if angle_hori >= 180:
                angle_hori = 180
            print(int(servo / 10))
            success, img = self.cap.read()
            if option == "sport":
                img = self.hand_detector.findHands(img, draw=True)
                object_hand = self.hand_detector.findPosition(img, draw=True)
                img, object_face = self.face_detector.findFaceMesh(img)
            if option == "show":
                img = self.hand_detector.findHands(img, draw=True)
                object_hand = self.hand_detector.findPosition(img, draw=True)
            if len(object_hand) != 0:
                hori = (np.mean([i[1] for i in object_hand]))
                verti = np.mean([i[2] for i in object_hand])
                hori_save = hori
                verti_save = verti
            if len(object_face) != 0:
                hori = (np.mean([i[0] for i in object_face[0]]))
                verti = np.mean([i[1] for i in object_face[0]])
                hori_save = hori
                verti_save = verti
            if len(object_hand) == 0 & len(object_face) == 0:
                hori = hori_save
                verti = verti_save
            else:
                pass
            self.cTime = time.time()
            fps = 1 / (self.cTime - self.pTime)
            self.pTime = self.cTime
            cv2.putText(img, f'FPS: {int(fps)}', (20, 70), cv2.FONT_HERSHEY_PLAIN,
                        3, (0, 255, 0), 3)
            cv2.circle(img, (self.midWCam, self.midHCam), radius=10, color=(255, 0, 0), thickness=-1)
            cv2.rectangle(img, (hitbox["W"], hitbox["S"]), (hitbox["E"], hitbox["N"]), color=(255, 0, 0), thickness=4)

            cv2.circle(img, (int(hori), int(verti)), radius=10, color=(255, 0, 0), thickness=-1)
            cv2.line(img, (int(hori), int(verti)), (int(self.midWCam), int(self.midHCam)), (255, 0, 0), thickness=4)

            cv2.imshow("RoboVision", img)
            object = []
            if len(object_face) != 0:
                object = object_face
            if len(object_hand) != 0:
                object = object_hand
            board.digital[10].write(0)
            # board.digital[8].write( 0 )
            board.digital[11].write(0)
            # board.digital[9].write( 0 )
            if len(object) != 0:
                if verti > hitbox["N"]:
                    if servo % 10 == 0:
                        board.digital[10].write(1)
                        # board.digital[8].write( 0 )
                        angle_verti += 1
                        setAngle(board, 12, angle_verti)
                elif verti < hitbox["S"]:
                    if servo % 10 == 0:
                        board.digital[11].write(1)
                        # board.digital[9].write( 0 )
                        angle_verti -= 1
                        setAngle(board, 12, angle_verti)

                if hori > hitbox["E"]:
                    if servo % 10 == 0:
                        angle_hori -= 1
                        setAngle(board, 13, angle_hori)
                elif hori < hitbox["W"]:
                    if servo % 10 == 0:
                        angle_hori += 1
                        setAngle(board, 13, angle_hori)

            cv2.waitKey(1)


def setAngle(board, pin, angle):
    board.digital[pin].write(angle)
    # time.sleep(0.005)#0.015


# angle=0

# Horizontal SERVO
board.digital[13].mode = pyfirmata.SERVO
setAngle(board, 13, 90)

# Vertical SERVO
board.digital[12].mode = pyfirmata.SERVO
setAngle(board, 12, 90)

# Vertical STEPPER
board.digital[11].mode = pyfirmata.OUTPUT
# board.digital[9].mode=pyfirmata.OUTPUT
# board.digital[8].mode=pyfirmata.OUTPUT
board.digital[10].mode = pyfirmata.OUTPUT
"""while True:
    for i in range(0, 180):
        setAngle(board,6,i)
    for i in range(0, 180,-1):
        setAngle(board,6,i)"""

tracking = TrackingClass(1)
# sport
# show
tracking.recoring(option="sport")
