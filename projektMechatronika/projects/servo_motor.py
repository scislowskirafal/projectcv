import pyfirmata
import time


def setAngle(board, pin, angle):
    board.digital[pin].write(angle)
    time.sleep(0.005)  # 0.015


if __name__ == "__main__":
    port = ''
    board = pyfirmata.Arduino('COM3', baudrate=115200)
    board.digital[7].mode = pyfirmata.SERVO
    angle = 180

    board.digital[6].write(1)
    time.sleep(1)
    board.digital[6].write(0)
    time.sleep(1)
    board.servo_config(pin=6, min_pulse=100, max_pulse=10000, angle=0)
    while True:
        board.servo_config(6, angle=90)
        time.sleep(1)
        board.servo_config(6, angle=180)
        time.sleep(1)
        board.servo_config(6, angle=270)
        time.sleep(1)
        for i in range(0, 180):
            setAngle(board, 7, i)
