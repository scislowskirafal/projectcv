from computerVision.modules import \
    HandTrackingModule, \
    FaceMeshModule, \
    PoseModule
import cv2
import numpy as np

wCam, hCam = 640, 480
midWCam, midHCam = 320, 240
face_detector = FaceMeshModule.FaceMeshDetector(maxFaces=1)
hand_detector = HandTrackingModule.handDetector(maxHands=1)
pose_detector = PoseModule.poseDetector()
cap = cv2.VideoCapture(2)
option = "hand"

while True:
    success, img = cap.read()
    cv2.circle(img, (midWCam, midHCam), radius=10, color=(0, 255, 0), thickness=-1)
    cv2.circle(img, (midWCam, midHCam), radius=100, color=(255, 0, 0), thickness=1)
    horizontal, vertical = 0, 0
    if option == "face":
        img, object = face_detector.findFaceMesh(img)
        if len(object) != 0:
            horizontal = (np.mean([i[0] for i in object[0]]))
            vertical = np.mean([i[1] for i in object[0]])
    elif option == "hand":
        img = hand_detector.findHands(img, draw=True)
        object = hand_detector.findPosition(img, draw=True)
        if len(object) != 0:
            horizontal = (np.mean([i[1] for i in object]))
            vertical = np.mean([i[2] for i in object])
    elif option == "pose":
        img = pose_detector.findPose(img, draw=True)
        object = pose_detector.findPosition(img, draw=True)
        if len(object) != 0:
            horizontal = (np.mean([i[1] for i in object]))
            vertical = np.mean([i[2] for i in object])

    cv2.circle(img, (int(horizontal), int(vertical)), radius=10, color=(255, 0, 0), thickness=1)
    cv2.imshow("RoboVision", img)
    cv2.waitKey(1)
