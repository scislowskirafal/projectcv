import cv2
import mediapipe as mp
import time
import computerVision.modules.HandTrackingModule as htm

"""
This code saves an image in saving_photos directory
if a hand is detected.
"""


class DetectToSave:
    def __init__(self, n_camera):
        self.pTime = 0
        self.cTime = 0
        self.cap = cv2.VideoCapture(n_camera)
        self.detector = htm.handDetector()

    def detection(self):
        i = 0
        count = 0
        state_save = 0
        while True:
            state = 0
            success, img = self.cap.read()
            img = self.detector.findHands(img, draw=True)
            lmList = self.detector.findPosition(img, draw=False)
            if len(lmList) != 0:
                pass
                # print(lmList[4])
            self.cTime = time.time()
            fps = 1 / (self.cTime - self.pTime)
            self.pTime = self.cTime

            # cv2.putText(img, str(int(fps)), (10, 70), cv2.FONT_HERSHEY_PLAIN, 3,
            #           (255, 0, 255), 3)

            cv2.imshow("Image", img)
            if len(lmList) != 0:
                count += 1
                state = 1
                # if count%10==0:
                print(state, state_save)
                if state == 1 and state_save == 0:
                    i += 1
                    state = 1
                    state_save = 1
                    cv2.imwrite("computerVision/robotVision/ComponentsPart/saving_opencv/" + "f" + str(i) + ".jpg", img)
            else:
                state = 0
                state_save = 0

            cv2.waitKey(1)


detecting = DetectToSave(2)
detecting.detection()
