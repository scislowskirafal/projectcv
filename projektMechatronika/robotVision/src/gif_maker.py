import glob
from PIL import Image
import os
import re


def sorted_alphanumeric(data: list):
    """
    Naturalne sortowanie nazw pliku:
    ['f170.jpg', 'f027.jpg', 'f003.jpg']

    :param data: lista zawierająca nazwy plików w katalogu
    :returns: returns posortowaną naturalnie listę plików
    """
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(data, key=alphanum_key)


class MakingAGIF:
    def __init__(self, fp_in, fp_out):
        """
        Klasa wykonująca pliki *.gif.

        :param fp_in: ścieżka folderu zdjęć, input
        :param fp_out: ścieżka folderu gifów, output
        """
        self.fp_in = fp_in
        self.fp_out = fp_out

    def last_number(self):
        """
        Zwraca liczbę naturalną z nazwy ostatniego zdjęcia po sortowaniu naturalnym.

        :returns: ostatnie zdjęcie
        """
        i = 0
        if os.listdir(self.fp_out):
            i = os.listdir(self.fp_out)[-1]
            i = i[:-4]
            i = i[4:]
            i = int(i) + 1
        return i

    def make_a_gif(self, duration: int):
        """
        Wykonuje plik gif i zapisuje go w foldesze fp_out.
        Troszy plik 'gifs', jeżeli takowy nie istnieje.


        :param duration: interwał, odstęp między klatkami
        """
        is_exist = os.path.exists(self.fp_out)
        if not is_exist:
            os.makedirs(self.fp_out)
        if os.listdir(self.fp_in):
            i = str(self.last_number())
            if os.listdir(self.fp_out):
                photo = sorted_alphanumeric(os.listdir(self.fp_out))
                photo = photo[-1]
                photo = photo[:-4]
                photo = photo[4:]
                i = int(photo) + 1
            self.fp_in += "*.jpg"
            print(i)
            img, *imgs = [Image.open(f) for f in sorted_alphanumeric(glob.glob(self.fp_in))]
            img.save(fp=self.fp_out + r"\film" + str(i) + ".gif", format='GIF', append_images=imgs,
                     save_all=True, duration=duration, loop=0)


if __name__ == '__main__':
    """
    Just makes GIFs.
    """
    fp_in = "projektMechatronika/robotVision/tests/saving_photos/"
    fp_out = "projektMechatronika/robotVision/tests/gifs"
    my_gif = MakingAGIF(fp_in=fp_in, fp_out=fp_out)
    my_gif.make_a_gif(duration=200)
