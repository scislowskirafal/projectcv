from projektMechatronika.modules import \
    HandTrackingModule, \
    FaceMeshModule, \
    PoseModule
import cv2
import numpy as np
import pyfirmata
import time
from sys import platform
import os
import shutil
import re


def clear_folder(folder):
    for filename in os.listdir(folder):
        file_path = os.path.join(folder, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))


def sorted_alphanumeric(data):
    """
    Naturalne sortowanie nazw pliku:
    ['f170.jpg', 'f027.jpg', 'f003.jpg']

    :param data: lista zawierająca nazwy plików w katalogu
    :returns: returns posortowaną naturalnie listę plików
    """
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(data, key=alphanum_key)


class TrackingClass:
    def __init__(self, n_camera):
        """
        Klasa służąca do śledzenia obiektów i tworzenia slajdów do gifów pokazowych.
        """
        self.wCam, self.hCam = 640, 480
        self.midWCam, self.midHCam = 320, 240
        self.cap = cv2.VideoCapture(n_camera)
        self.face_detector = FaceMeshModule.FaceMeshDetector(maxFaces=1)
        self.hand_detector = HandTrackingModule.handDetector(maxHands=1)
        self.pose_detector = PoseModule.poseDetector()
        self.pTime = 0
        self.cTime = 0

    def recoring(self, option=''):
        verti_save = 90
        hori_save = 90
        angle_verti = 90
        angle_hori = 90
        servo = 0
        i = 0
        folder = "projektMechatronika/robotVision/tests/saving_photos/"
        is_exist = os.path.exists(folder)
        """if is_exist:
            clear_folder(folder)"""
        if not is_exist:
            os.makedirs(folder)
        while True:
            servo += 10
            if angle_verti <= 1:
                angle_verti = 1
            if angle_verti >= 180:
                angle_verti = 180
            if angle_hori <= 1:
                angle_hori = 1
            if angle_hori >= 180:
                angle_hori = 180
            print(int(servo / 10))
            _, img = self.cap.read()
            if option == 'face':
                img, object = self.face_detector.findFaceMesh(img)
                # middle=object[31]
                if len(object) != 0:
                    hori = (np.mean([i[0] for i in object[0]]))
                    verti = np.mean([i[1] for i in object[0]])
                    hori_save = hori
                    verti_save = verti
                if len(object) == 0:
                    hori = hori_save
                    verti = verti_save
            elif option == 'hand':
                img = self.hand_detector.findHands(img, draw=True)
                object = self.hand_detector.findPosition(img, draw=True)
                if len(object) != 0:
                    hori = (np.mean([i[1] for i in object]))
                    verti = np.mean([i[2] for i in object])
                    hori_save = hori
                    verti_save = verti
                if len(object) == 0:
                    hori = hori_save
                    verti = verti_save
            if option == 'pose':
                img = self.pose_detector.findPose(img, draw=True)
                object = self.pose_detector.findPosition(img, draw=True)
                if len(object) != 0:
                    hori = (np.mean([i[1] for i in object]))
                    verti = np.mean([i[2] for i in object])
                    hori_save = hori
                    verti_save = verti
                if len(object) == 0:
                    hori = hori_save
                    verti = verti_save
            else:
                pass
            self.cTime = time.time()
            fps = 1 / (self.cTime - self.pTime)
            self.pTime = self.cTime
            cv2.putText(img, f'FPS: {int(fps)}', (20, 70), cv2.FONT_HERSHEY_PLAIN,
                        3, (0, 255, 0), 3)
            cv2.circle(img, (self.midWCam, self.midHCam), radius=10, color=(255, 0, 0), thickness=-1)
            cv2.rectangle(img, (self.midWCam - 100, self.midHCam - 100), (self.midWCam + 100, self.midHCam + 100),
                          color=(255, 0, 0), thickness=4)

            cv2.circle(img, (int(hori), int(verti)), radius=10, color=(255, 0, 0), thickness=-1)
            cv2.line(img, (int(hori), int(verti)), (int(self.midWCam), int(self.midHCam)), (255, 0, 0), thickness=4)

            cv2.imshow("RoboVision", img)

            board.digital[10].write(0)
            board.digital[11].write(0)
            if len(object) != 0:
                if verti > 340:
                    if servo % 30 == 0:
                        board.digital[10].write(1)
                        angle_verti += 1
                        set_angle(board, 12, angle_verti)
                elif verti < 140:
                    if servo % 30 == 0:
                        board.digital[11].write(1)
                        angle_verti -= 1
                        set_angle(board, 12, angle_verti)

                if hori > 420:
                    if servo % 10 == 0:
                        angle_hori -= 1
                        set_angle(board, 13, angle_hori)
                elif hori < 220:
                    if servo % 10 == 0:
                        angle_hori += 1
                        set_angle(board, 13, angle_hori)
            j = ""
            if servo % 100 == 0:
                i += 1
                cv2.imwrite(folder + "f" + j + str(i) + ".jpg", img)
            cv2.waitKey(1)


def set_angle(board, pin, angle):
    board.digital[pin].write(angle)


print("test1")
board = pyfirmata.Arduino('COM3')
print("test2")

# Horizontal SERVO
board.digital[13].mode = pyfirmata.SERVO
set_angle(board, 13, 90)

# Vertical SERVO
board.digital[12].mode = pyfirmata.SERVO
set_angle(board, 12, 90)

# Vertical STEPPER
board.digital[11].mode = pyfirmata.OUTPUT
board.digital[10].mode = pyfirmata.OUTPUT

tracking = TrackingClass(2)
tracking.recoring('hand')
