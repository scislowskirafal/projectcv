import time
from sys import platform
import os
import re

if platform == "armv7l" or platform == "armv6l":
    from picamera import Picamera
else:
    import cv2


def sorted_alphanumeric(data: list):
    """
    Naturalne sortowanie nazw pliku:
    ['f170.jpg', 'f027.jpg', 'f003.jpg']

    :param data: lista zawierająca nazwy plików w katalogu
    :returns: returns posortowaną naturalnie listę plików
    """
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(data, key=alphanum_key)


class SavingPhotos:
    def __init__(self, duration: int, n_photos: int, n_camera: int):
        """
        Klasa wykonująca zdjęcia i zapisująca je w folderze saving_photos.

        :param duration: długość timelapsa
        :param n_photos: liczba zdjęć
        :param n_camera: numer kamery, lub ścieżka do filmu
        """
        self.start = time.time()
        self.duration = duration
        self.n_photos = n_photos
        if platform == "armv7l" or platform == "armv6l":
            pass
        else:
            self.cap = cv2.VideoCapture(n_camera)

    def end_time(self):
        return self.start + self.duration

    def saving_photo(self, finite: bool, limit: int = 100):
        """
        Moduł wykonujący zdjęcia.
        Tworzy plik 'saving_photos', jeżeli takiego nie ma.

        :param limit: maksymalna liczba klatek w trybie infinite
        :param finite:
        :returns: this is a description of what is returned
        """

        period = self.duration / self.n_photos
        folder = "projektMechatronika/robotVision/tests/saving_photos/"
        is_exist = os.path.exists(folder)
        if not is_exist:
            os.makedirs(folder)
        j = 0
        if os.listdir(folder):
            photo = sorted_alphanumeric(os.listdir(folder))
            photo = photo[-1]
            photo = photo[:-4]
            photo = photo[1:]
            j = int(photo) + 1
        if not finite:
            self.n_photos = limit
        for i in range(self.n_photos):
            print("siema")
            if platform == "armv7l" or platform == "armv6l":
                pass
            else:
                self.photo(i + int(j), folder)
            time.sleep(period)

    def photo(self, i: int, folder: str):
        """
        Moduł zapisujący zdjęcia do specjalnego folderu.

        :param i: iteracja, następne zdjęcie
        :param folder: ścieżka do folderu saving_photos
        """
        _, img = self.cap.read()
        j = ""
        cv2.imwrite(folder + "f" + j + str(i) + ".jpg", img)


if __name__ == "__main__":
    """
    Robienie i zapisywanie zdjęć.
    """
    clock = SavingPhotos(duration=10, n_photos=10, n_camera=2)
    clock.saving_photo(finite=False, limit=5)
